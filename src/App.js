import './App.scss';
import { Navigate, Routes, Route } from "react-router-dom";
import { useSelector } from "react-redux";

import Footer from "./components/Footer";
import Header from "./components/Header";
import Login from "./pages/Login";
import PropertiesList from "./pages/PropertiesList";

const App = () => {
  return (
    <>
      <div className="content">
        <Header />

        <div className="main-content">
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/properties" element={<PropertiesList />} />
            <Route path="*" element={<Navigate to="/properties" replace />} />
          </Routes>
        </div>
      </div>

      <Footer />
    </>
  );
}

export default App;
