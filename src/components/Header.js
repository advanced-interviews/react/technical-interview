import React from "react";
import { Link } from "react-router-dom";

import { ReactComponent as LogoIcon } from "../images/logo.svg";

const Header = (props) => {
  return (
    <header>
      <div>
        <LogoIcon className="logo" />
      </div>
      <div>
        <Link
          to="/properties"
        >
          View Properties
        </Link>
        <Link
          to="/login"
          className="btn"
        >
          Login
        </Link>
      </div>
    </header>
  )
}

export default Header;
