const initialState = {
  token: null,
  user: null
}

const appReducer = (state = initialState, action) => {
  switch (action.type) {

    default:
      return state;
  }
}

export default appReducer;
