1. Checkout repository, install dependancies and start the application
2. Change the phone number in the footer to be a clickable link.
3. Change the email address in the footer to be a clickable link.
4. Complete the src/pages/PropertiesList page. It should:
    **NOTE - Take this opportunity to show us your creative skills. A great design is a bonus.**
    1. Retrieve a list of properties from (GET) https://backend.interview.advanced.co.uk/api/properties.
    2. Whilst the request is loading, show a loading animation.
    3. Display the retrieved properties in a grid view
    4. Each property should show:
        1. Property image
        2. Property address
        3. Number of bedrooms, bathrooms and living rooms
        4. Property description 
        5. Property price
        6. Button to view property details
    5. Property list page should be responsive on desktop, tablet and mobile.
5. Complete the src/pages/Login page. It should:
    1. Consist of 2 input fields (email and password) and a ‘Login’ button.
    2. When the ‘Login’ button is clicked, send a ‘POST’ request to https://backend.interview.advanced.co.uk/api/login.
    3. Whilst the request is loading, show a loading animation.
    4. If a successful response is received, store the received token and user object in the Redux state and redirect the user to the PropertyListPage.
    5. If the login attempt failed, show an error message.
    6. Login page should be responsive on desktop, tablet and mobile.
6. If a user is logged in, they should:
    1. See their first name in the top left hand corner of the page.
    2. See a ‘Logout’ button next to their name, which when clicked will clear the token and user object in the Redux state.
    3. See an additional button on the property list, this button should display ’Submit Offer’.
7. Ensure the whole application is responsive and works on desktop, tablet and mobile.
